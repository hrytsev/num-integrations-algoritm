function thirdF(x, y) {
    return  x * Math.cos(x * x + y);
}

function calculate(X0, Y0, B, N, method, outID) {
    const H = (B - X0) / N;
    const H2 = H / 2;
    let X = X0;
    let Z = Y0;
    let output = "";

    for (let i = 1; i <= N; i++) {
        let Y = Z;
        let F = thirdF(X, Y);
        let V, P, R, K1, K2, K3, K4;

        switch (method) {
            case 'Euler':
                Y = Z + H * F;
                break;
            case 'EulerCauchy':
                V = F;
                Y = Z + H * F;
                F = thirdF(X + H, Y);
                Y = Z + H2 * (V + F);
                break;
            case 'ImprovedEuler':
                let W = Z;
                while (true) {
                    V = W;
                    F = thirdF(X + H, Y);
                    P = Z + H2 * (V + F);
                    R = P - W;
                    if (Math.abs(R) < 0.1 * 10 ** -6) {
                        Y = P;
                        Z = P;
                        break;
                    } else {
                        W = P;
                    }
                }
                break;
            case 'RungeKutta':
                K1 = H * F;
                K2 = H * thirdF(X + H2, Z + K1 / 2);
                K3 = H * thirdF(X + H2, Z + K2 / 2);
                K4 = H * thirdF(X + H, Z + K3);
                Y = Z + (K1 + 2 * (K2 + K3) + K4) / 6;
                break;
            default:
                console.log("wrong param")
                break;
        }

        X += H;
        output += `(${X.toFixed(2)}, ${Y.toFixed(6)})<br>`;
        Z = Y;
    }

    document.getElementById(outID).innerHTML = output;
}

calculate(0.5, 2, 1, 10, 'Euler', 'eulerOutput');
calculate(0.5, 2, 1, 10, 'EulerCauchy', 'eulerCauchyOutput');
calculate(0.5, 2, 1, 10, 'ImprovedEuler', 'improvedEulerOutput');
calculate(0.5, 2, 1, 10, 'RungeKutta', 'rungeKuttaOutput');
